﻿using System;
using UnityEngine;

namespace FuzzyLogic
{
    public static class FuzzyLogicUtilities
    {
        public static float LeftShoulder(float x, float a, float b)
        {
            if (x is < 0.0f or > 1.0f)
            {
                throw new Exception("You should have 0 <= x <= 1");
            }

            if (!(0 <= a && a < b && b <= 1))
            {
                throw new Exception("You should have 0 <= a < b <= 1");
            }
            // if (x <= a)
            // {
            //     return 1.0f;
            // }
            //
            // if (x <= b)
            // {
            //     return (b - x) / (b - a);
            // }
            // return 0.0f;
            return Mathf.Clamp((b - x) / (b - a), 0.0f, 1.0f);
        }

        public static float RightShoulder(float x, float a, float b)
        {
            // y = (x - a) / (b - a)
            if (x is < 0.0f or > 1.0f)
            {
                throw new Exception("You should have 0 <= x <= 1");
            }
            if (!(0 <= a && a < b && b <= 1))
            {
                throw new Exception("You should have 0 <= a < b <= 1");
            }

            // if (x >= b)
            // {
            //     return 1.0f;
            // }
            //
            // if (x < a)
            // {
            //     return 0.0f;
            // }
            //
            // return (x - a) / (b - a);
            //
            return Mathf.Clamp((x - a) / (b - a), 0.0f, 1.0f);
        }

        public static float Triangular(float x, float a, float b, float c)
        {
            if (x is < 0.0f or > 1.0f)
            {
                throw new Exception("You should have 0 <= x <= 1");
            }
            if (!(0 <= a && a < b && b < c && c <= 1))
            {
                throw new Exception("You should have 0 <= a < b <= 1");
            }
            
            if (Mathf.Approximately(x, b))
            {
                return 1.0f;
            }
            
            if (x < a || x > c)
            {
                return 0.0f;
            }
            
            if (x < b)
            {
                return RightShoulder(x, a, b);
            }
            
            return LeftShoulder(x, b, c);
            
            // Other Way
            // return Trapezoidal(x, a, b, b, c);
        }

        public static float Trapezoidal(float x, float a, float b, float c, float d)
        {
            if (x is < 0.0f or > 1.0f)
            {
                throw new Exception("You should have 0 <= x <= 1");
            }
            if (!(0 <= a && a < b && b <= c && c < d && d <= 1))
            {
                throw new Exception("You should have 0 <= a < b <= 1");
            }

            if (x >= b && x <= c)
            {
                return 1.0f;
            }

            if (x < a || x > d)
            {
                return 0.0f;
            }

            if (x < b)
            {
                return RightShoulder(x, a, b);
            }

            return LeftShoulder(x, b, c);
        }
        
        
    }
}