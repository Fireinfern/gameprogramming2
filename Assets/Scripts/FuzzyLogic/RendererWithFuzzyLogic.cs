﻿using System;
using UnityEngine;

namespace FuzzyLogic
{
    public class RendererWithFuzzyLogic : MonoBehaviour
    {
        private LineRenderer _lineRenderer;

        private void Start()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            for (float i = 0.0f; i <= 10.0f; i++)
            {
                var step = i / 10.0f;
                var yValue = FuzzyLogicUtilities.Triangular(step, 0.20f, 0.6f, 0.8f);
                _lineRenderer.SetPosition((int)i, new Vector3(step, yValue));
            }
        }
    }
}