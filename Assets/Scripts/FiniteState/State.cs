﻿using System.Collections.Generic;
using UnityEngine;

namespace FiniteState
{
    
    public class State : ScriptableObject
    {

        [SerializeField] private List<Transition> transitions = new List<Transition>();

        virtual public void UpdateState(Blackboard blackboard)
        {
            
        }

        virtual public void OnEnteredState(Blackboard blackboard)
        {
            
        }

        virtual public void OnLeaveState(Blackboard blackboard)
        {
            
        }

        public State CanTransition(Blackboard blackboard)
        {
            foreach (var transition in transitions)
            {
                if (transition.CanMakeTransition(blackboard))
                {
                    return transition.TransitionToState;
                }
            }
            
            return null;
        }
    }
}