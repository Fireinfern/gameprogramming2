﻿using UnityEngine;

namespace FiniteState.Transitions
{
    [CreateAssetMenu(fileName = "TransformTransition", menuName = "Transitions/Transform", order = 5)]
    public class SingleTransformVariableTransition : Transition
    {

        [SerializeField] protected string variableName = "";
        
        public override bool CanMakeTransition(Blackboard blackboard)
        {
            return blackboard.GetTransformVariableByName(variableName)?.value;
        }
    }
}