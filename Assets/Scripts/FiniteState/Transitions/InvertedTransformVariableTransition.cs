﻿using UnityEngine;

namespace FiniteState.Transitions
{
    [CreateAssetMenu(fileName = "InverseTransformTransition", menuName = "Transitions/InverseTransform", order = 5)]
    public class InvertedTransformVariableTransition: SingleTransformVariableTransition
    {
        public override bool CanMakeTransition(Blackboard blackboard)
        {
            return !blackboard.GetTransformVariableByName(variableName)!.value;
        }
    }
}