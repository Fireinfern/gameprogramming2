﻿using UnityEngine;

namespace FiniteState.Transitions
{
    [CreateAssetMenu(fileName = "BoolTransition", menuName = "Transitions/Bool", order = 5)]
    public class SingleBoolVariableTransition : Transition
    {

        [SerializeField] private string variableName = "";
        
        public override bool CanMakeTransition(Blackboard blackboard)
        {
            var variable = blackboard.GetBoolVariableByName(variableName);
            return variable is {value: true};
        }
    }
}