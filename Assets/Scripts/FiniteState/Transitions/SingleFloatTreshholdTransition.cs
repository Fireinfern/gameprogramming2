﻿using UnityEngine;

namespace FiniteState.Transitions
{
    [CreateAssetMenu(fileName = "FloatTransition", menuName = "Transitions/Float", order = 5)]
    public class SingleFloatTreshholdTransition: Transition
    {
        [SerializeField] private string variableName = "";

        [SerializeField] private float minimum = 0.0f;
        
        public override bool CanMakeTransition(Blackboard blackboard)
        {
            return blackboard.GetFloatVariableByName(variableName)?.value <= minimum;
        }
    }
}