﻿using UnityEngine;

namespace FiniteState.States
{
    [CreateAssetMenu(fileName = "DeadState", menuName = "States/Dead", order = 0)]
    public class DeadState : State
    {
        
    }
}