﻿using UnityEngine;

namespace FiniteState.States
{
    [CreateAssetMenu(fileName = "Follow", menuName = "States/Follow", order = 0)]
    public class FollowState : State
    {
        [SerializeField] private string transformVariableName = "target";

        [SerializeField] private float speed = 10.0f;
        
        public override void UpdateState(Blackboard blackboard)
        {
            var targetTransformValue = blackboard.GetTransformVariableByName(transformVariableName);
            if (targetTransformValue == null) return;
            var targetTransform = targetTransformValue.value;
            var movement = Vector3.MoveTowards(blackboard.transform.position, targetTransform.position, speed * Time.deltaTime);
            blackboard.transform.position = movement;
        }
    }
}