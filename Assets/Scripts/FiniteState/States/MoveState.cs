﻿using UnityEngine;

namespace FiniteState.States
{
    [CreateAssetMenu(fileName = "MoveState", menuName = "States/Move", order = 2)]
    public class MoveState : State
    {
        [SerializeField] private float moveTime = 5.0f;

        [SerializeField] private float movementSpeed = 10.0f;
        
        private float _remainingMoveTime = 0.0f;


        public override void UpdateState(Blackboard blackboard)
        {
            base.UpdateState(blackboard);
            _remainingMoveTime -= Time.deltaTime;
            if (_remainingMoveTime <= 0.0f)
            {
                blackboard.SetBoolVariableByName("FinishMoving", true);
                return;
            }

            Vector3 forward =blackboard.transform.forward * (movementSpeed * Time.deltaTime);
            blackboard.transform.Translate(forward);
        }

        public override void OnEnteredState(Blackboard blackboard)
        {
            _remainingMoveTime = moveTime;
            blackboard.SetBoolVariableByName("FinishMoving", false);
        }
    }
}