﻿using UnityEngine;

namespace FiniteState.States
{
    [CreateAssetMenu(fileName = "Idle", menuName = "States/Idle", order = 0)]
    public class IdleState : State
    {
        [SerializeField] private float timeToIdle = 5.0f;

        private float _timeLeftToIdle = 0.0f;

        public override void UpdateState(Blackboard blackboard)
        {
            blackboard.transform.Rotate(Vector3.up, Time.deltaTime * 20.0f);
            _timeLeftToIdle -= Time.deltaTime;
            if (_timeLeftToIdle <= 0.0f)
            {
                blackboard.SetBoolVariableByName("FinishIdling", true);
            }
        }

        public override void OnEnteredState(Blackboard blackboard)
        {
            _timeLeftToIdle = timeToIdle;
            blackboard.SetBoolVariableByName("FinishIdling", false);
        }
    }
}