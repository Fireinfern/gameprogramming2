﻿using UnityEngine;

namespace FiniteState.States
{
    [CreateAssetMenu(fileName = "SearchPlayerState", menuName = "States/Search", order = 5)]
    public class SearchPlayerState : State
    {
        [SerializeField] private float timeToFinishSearch = 5.0f;

        private float _timeRemaining = 0.0f;
        
        public override void UpdateState(Blackboard blackboard)
        {
            blackboard.transform.Rotate(Vector3.up, Time.deltaTime * 5.0f);
            _timeRemaining -= Time.deltaTime;
            blackboard.SetFloatVariableByName("searchingTime", _timeRemaining);
        }

        public override void OnEnteredState(Blackboard blackboard)
        {
            _timeRemaining = timeToFinishSearch;
            blackboard.SetFloatVariableByName("searchingTime", _timeRemaining);
        }
    }
}