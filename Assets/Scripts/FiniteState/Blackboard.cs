﻿using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace FiniteState
{

    [Serializable]
    public class BlackboardVariable<T>
    {
        public string name;
        public T value;

        public BlackboardVariable(string newName, T newValue)
        {
            name = newName;
            value = newValue;
        }
    }

    public class Blackboard : MonoBehaviour
    {
        [SerializeField] private List<BlackboardVariable<float>> floatValues = new List<BlackboardVariable<float>>();

        [SerializeField] private List<BlackboardVariable<bool>> boolValues = new List<BlackboardVariable<bool>>();

        [SerializeField] private List<BlackboardVariable<Transform>> transformValues = new List<BlackboardVariable<Transform>>();

        public BlackboardVariable<float> GetFloatVariableByName(string searchedName)
        {
            foreach (var variable in floatValues)
            {
                if (variable.name == searchedName)
                {
                    return variable;
                }
            }
            return null;
        }
        
        public void SetFloatVariableByName(string searchedName, float newValue)
        {
            for (int i = 0; i < floatValues.Count; i++)
            {
                if (floatValues[i].name == searchedName)
                {
                    floatValues[i].value = newValue;
                }
            } 
        }

        public BlackboardVariable<bool> GetBoolVariableByName(string searchedName)
        {
            foreach (var variable in boolValues)
            {
                if (variable.name == searchedName)
                {
                    return variable;
                }
            }

            return null;
        }

        public void SetBoolVariableByName(string searchedName, bool newValue)
        {
            for (var index = 0; index < boolValues.Count; index++)
            {
                var t = boolValues[index];
                if (t.name == searchedName)
                {
                    t.value = newValue;
                }
            }
        }
        
        public BlackboardVariable<Transform> GetTransformVariableByName(string searchedName)
        {
            foreach (var variable in transformValues)
            {
                if (variable.name == searchedName)
                {
                    return variable;
                }
            }

            return null;
        }

        public void SetTransformVariableByName(string searchedName, Transform newValue)
        {
            for (var index = 0; index < transformValues.Count; index++)
            {
                var t = transformValues[index];
                if (t.name == searchedName)
                {
                    t.value = newValue;
                }
            }
        }
    }
}