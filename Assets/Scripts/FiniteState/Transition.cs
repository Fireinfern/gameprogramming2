﻿using System;
using UnityEngine;

namespace FiniteState
{
    [Serializable]
    public class Transition : ScriptableObject
    {

        [SerializeField] private State transitionToState;

        public State TransitionToState => transitionToState;
        
        virtual public bool CanMakeTransition(Blackboard blackboard)
        {
            return true;
        }
    }
}