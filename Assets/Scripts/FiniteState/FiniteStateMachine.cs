﻿using System;
using UnityEngine;

namespace FiniteState
{
    [RequireComponent(typeof(Blackboard))]
    public class FiniteStateMachine : MonoBehaviour
    {

        [SerializeField]
        private State initialState;
        
        private State _currentState = null;

        private Blackboard _blackboard;

        private void Start()
        {
            _currentState = initialState;
            _blackboard = GetComponent<Blackboard>();
            _currentState.OnEnteredState(_blackboard);
        }

        private void Update()
        {
            if (!_currentState) return;
            var newState = _currentState.CanTransition(_blackboard);
            if (newState)
            {
                _currentState.OnLeaveState(_blackboard);
                _currentState = newState;
                _currentState.OnEnteredState(_blackboard);
            }
            _currentState.UpdateState(_blackboard);
        }
        
        
    }
}