using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

enum State : uint
{   
    None = 0,
    Idle = 1,
    Move = 2,
    Follow = 3,
    Search = 4,
    Dead = 5,
}

public class BasicFiniteStateMachine : MonoBehaviour
{
    [SerializeField]
    private State currentState = State.None;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _hasDetectedPlayer = true;
            _targetTransform = other.transform;
        }
    }
    
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _hasDetectedPlayer = false;
            _targetTransform = null;
        }
    }

    // Blackboard Variables

    private bool _hasDetectedPlayer = false;

    private float _cooldonwRemainingTime = 0.0f;

    private Transform _targetTransform;

    private float _timeToFinishSearching = 0.0f;

    private float _timeToRotate = 0.0f;
    // StatesHandles
    
    private void HandleNoneState()
    {
        // Transition to State Idle
        currentState = State.Idle;
        _cooldonwRemainingTime = 5.0f;
    }

    private void HandleIdleState()
    {
        // verify if you can transition to any state
        if (_hasDetectedPlayer)
        {
            currentState = State.Follow;
            return;
        }

        if (_cooldonwRemainingTime <= 0.0f)
        {
            currentState = State.Move;
            _timeToRotate = 20.0f;
            return;
        }
        transform.Rotate(Vector3.up, Time.deltaTime * 20.0f);
        _cooldonwRemainingTime -= Time.deltaTime;
    }

    private void HandleFollowState()
    {
        if (_targetTransform)
        {
            float distance = Vector3.Distance(transform.position, _targetTransform.position);
            if (5.0f < distance)
            {
                transform.position = Vector3.MoveTowards(transform.position, _targetTransform.position, 1.0f);
            }
            transform.rotation = Quaternion.LookRotation((_targetTransform.position - transform.position));
        }
        else
        {
            currentState = State.Search;
            _timeToFinishSearching = 10.0f;
        }
    }

    private void HandleSearchPlayerState()
    {
        if (_targetTransform)
        {
            currentState = State.Follow;
            return;
        }

        if (_timeToFinishSearching <= 0.0f)
        {
            _timeToFinishSearching = 0.0f;
            currentState = State.Move;
            return;
        }

        _timeToFinishSearching -= Time.deltaTime;
        transform.Rotate(Vector3.up, Time.deltaTime * 20.0f);
    }

    private void HandleMoveState()
    {
        _timeToRotate -= Time.deltaTime;
        if (_timeToRotate <= 0.0f)
        {
            currentState = State.Idle;
            return;
        }
        Vector3 newTransform = transform.forward * (5.0f * Time.deltaTime);
        transform.Translate(newTransform);
    }
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case State.Idle:
                HandleIdleState();
                break;
            case State.Follow:
                HandleFollowState();
                break;
            case State.Move:
                HandleMoveState();
                break;
            case State.Search:
                HandleSearchPlayerState();
                break;
            case State.Dead:
                break;
            default:
                HandleNoneState();
                break;
        }
    }
}
