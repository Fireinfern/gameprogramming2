using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PerformOperations();
    }

    void PerformOperations()
    {
        Vector3 v1 = new Vector3(1.5f, 2.5f, 3.5f);
        Vector3 v2 = new Vector3(1.0f, -3.5f, 1.5f);
        float K = 0.75f;

        // v1 + v2 = (2.5f, -1.0f, 5.0f)
        Vector3 solution1 = v1 + v2;
        Debug.Log(solution1);
        
        // v1 - v2 = (0.5f, 6.0f, 2.0f)
        Vector3 solution2 = v1 - v2;
        Debug.Log(solution2);

        // K * v1 = (1.125f, 1.875f, 2.625f)
        Vector3 solution3 = K * v1;
        Debug.Log(solution3);

        // K * v2 = (0.75f, -2.625f, 1.125f)
        Vector3 solution4 = K * v2;
        Debug.Log(solution4);
        
        // v1 . v2 = -2
        float solution5 = Vector3.Dot(v1, v2);
        Debug.Log(solution5);

        // v1 X v2 = (16.0f, 1.25f, -7.75f)
        Vector3 solution6 = Vector3.Cross(v1, v2);
        Debug.Log(solution6);

        // 4.55217
        float solution7 = v1.magnitude;
        Debug.Log(solution7);

        // 3.937004
        float solution8 = v2.magnitude;
        Debug.Log(solution8);

        // 96.40298
        float solution9 = Vector3.Angle(v1, v2);
        float solution10 = solution5 / (solution7 * solution8);
        solution10 = Mathf.Acos(solution10) * Mathf.Rad2Deg;
        Debug.Log(solution9);
        Debug.Log(solution10);
        Debug.Assert(Mathf.Approximately(solution9, solution10));
    }
}
