﻿using System;
using FiniteState;
using UnityEngine;


public class FiniteStatePicker : MonoBehaviour
{

    [SerializeField] private bool useImprovedFSM = false;

    [SerializeField] private BasicFiniteStateMachine basicFiniteStateMachine;

    [SerializeField] private FiniteStateMachine finiteStateMachine;

    private void Start()
    {
        basicFiniteStateMachine.enabled = !useImprovedFSM;
        finiteStateMachine.enabled = useImprovedFSM;
    }
}